<?php

use \App\BusinessLogic\PriceHistory\PriceHistoryService;
use \App\BusinessLogic\Certificates\CertificatesService;

class PriceHistoryServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var PriceHistoryService
     */
    protected $priceHistory;

    /**
     * @var CertificatesService
     */
    protected $certificateService;

    /**
     * @throws \Codeception\Exception\ModuleException
     */
    protected function _before()
    {
        $this->priceHistory = $this->getModule('Symfony')->_getContainer()->get('App\BusinessLogic\PriceHistory\PriceHistoryService');
        $this->certificateService = $this->getModule('Symfony')->_getContainer()->get('App\BusinessLogic\Certificates\CertificatesService');

    }

    /**
     * Add certificate prices
     */
    public function testAddCertificatePriceHistory()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
        );
        $certificate = $this->certificateService->addCertificates($fields);
        $params = array(
            'current_price' => 15.10,
            'issuing_price' => 20.10,
            'certificate' => $certificate['data']
        );
        $response = $this->priceHistory->addCertificatePrice($params);
        $this->tester->assertEquals(201, $response['status']);

    }
}