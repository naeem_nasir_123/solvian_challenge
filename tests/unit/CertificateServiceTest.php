<?php

use \App\BusinessLogic\Certificates\CertificatesService;

class CertificateServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var CertificatesService
     */
    protected $certificateService;

    /**
     * @throws \Codeception\Exception\ModuleException
     */
    protected function _before()
    {
        $this->certificateService = $this->getModule('Symfony')->_getContainer()->get('App\BusinessLogic\Certificates\CertificatesService');

    }

    /**
     * Add Standard certificate
     */
    public function testAddStandardCertificate()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(201, $response['status']);
    }

    /**
     * Add Bonus certificate
     */
    public function testAddBonusCertificate()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'barrier_level' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(201, $response['status']);
    }

    /**
     * Add Guarantee certificate
     */
    public function testAddGuarenteeCertificate()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(201, $response['status']);
    }

    /**
     * Add Standard with missing ISIN
     */
    public function testAddCertificateButIsinIsMissing()
    {
        $fields = array(
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with missing Trading market
     */
    public function testAddCertificateButTradingMarketIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with missing Issuer
     */
    public function testAddCertificateButIssuerIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with missing Currency
     */
    public function testAddCertificateButCurrencyIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with missing Current Price
     */
    public function testAddCertificateButCurrentPriceIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'issuing_price' => 5.10,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with missing Issuing price
     */
    public function testAddCertificateButIssuingPriceIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'participation_rate' => '10.10'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Add Standard with Document
     */
    public function testAddCertificateWithDocument()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 10.0,
            'participation_rate' => '10.10',
            'document_name' => 'test document',
            'document_type' => 'finantial'
        );
        $response = $this->certificateService->addCertificates($fields);
        $this->tester->assertEquals(201, $response['status']);
    }

    /**
     * Remove Certificate
     */
    public function testRemoveCertificate()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 10.0,
            'participation_rate' => '10.10',
            'document_name' => 'test document',
            'document_type' => 'finantial'
        );
        $certificate = $this->certificateService->addCertificates($fields);
        $response = $this->certificateService->removeCertificate(array('id' => $certificate['data']->getId()));
        $this->tester->assertEquals(202, $response['status']);
    }

    /**
     * Remove Certificate with missing id
     */
    public function testRemoveCertificateButIdIsMissing()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 10.0,
            'participation_rate' => '10.10',
            'document_name' => 'test document',
            'document_type' => 'finantial'
        );
        $this->certificateService->addCertificates($fields);
        $response = $this->certificateService->removeCertificate(array());
        $this->tester->assertEquals(403, $response['status']);
    }

    /**
     * Remove a non existent Certificate
     */
    public function testRemoveAnInvalidCertificate()
    {
        $response = $this->certificateService->removeCertificate(array('id' => 404));
        $this->tester->assertEquals(404, $response['status']);
    }

    /**
     * Get Certificate by id
     */
    public function testGetCertifiicateById()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
        );
        $insertedCertificate = $this->certificateService->addCertificates($fields);
        $certificate = $this->certificateService->getCertificates($insertedCertificate['data']->getId());
        $this->tester->assertEquals($insertedCertificate['data']->getId(), $certificate->getId());
    }

    /**
     * Get all Certificates
     */
    public function testGetAllCertifiicate()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
        );
        $this->certificateService->addCertificates($fields);
        $certificate = $this->certificateService->getAllCertificates();
        $this->tester->assertEquals(3, count($certificate));
    }
}