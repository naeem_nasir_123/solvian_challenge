<?php

namespace App\Repository;

use App\Entity\PriceHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PriceHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceHistory[]    findAll()
 * @method PriceHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PriceHistory::class);
    }

    /**
     * @param array $params
     * @return array|null
     */
    public function addPrice(array $params) :?array
    {
        try{
            $priceHistory = new PriceHistory();
            $priceHistory->setCertificateId($params['certificate']);
            $priceHistory->setIssuingPrice($params['issuing_price']);
            $priceHistory->setCurrentPrice($params['current_price']);
            $priceHistory->setPriceDateTime(new \DateTime('now'));
            $this->_em->persist($priceHistory);
            $this->_em->flush($priceHistory);

            return array(
                'status' => 201,
                'data' => "price added"
            );

        } catch (\Doctrine\ORM\ORMException | \Exception | \Doctrine\ORM\OptimisticLockException $ex){

            return array(
                'status' => 500,
                'data' => $ex->getMessage()
            );
        }
    }
}
