<?php

namespace App\Repository;

use App\Entity\CertificateDocuments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CertificateDocuments|null find($id, $lockMode = null, $lockVersion = null)
 * @method CertificateDocuments|null findOneBy(array $criteria, array $orderBy = null)
 * @method CertificateDocuments[]    findAll()
 * @method CertificateDocuments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CertificateDocumentsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CertificateDocuments::class);
    }

    /**
     * @param array $params
     * @return array|null
     */
    public function addDocument(array $params) :?array
    {
        try{
            $documents = new CertificateDocuments();
            $documents->setName($params['name']);
            $documents->setCertificateId($params['certificate']);
            $documents->setPath($params['path']);
            $documents->setDocumentType($params['document_type']);
            $documents->setCreatedAt(new \DateTime('now'));
            $this->_em->persist($documents);
            $this->_em->flush($documents);

            return array(
                'status' => 201,
                'data' => "document added"
            );
        } catch (\Doctrine\ORM\ORMException | \Exception | \Doctrine\ORM\OptimisticLockException $ex){

            return array(
                'status' => 500,
                'data' => $ex->getMessage()
            );
        }
    }
}
