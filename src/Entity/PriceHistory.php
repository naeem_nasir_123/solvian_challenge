<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PriceHistoryRepository")
 */
class PriceHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Certificates", inversedBy="priceHistories")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $certificate_id;

    /**
     * @ORM\Column(type="float")
     */
    private $issuing_price;

    /**
     * @ORM\Column(type="float")
     */
    private $current_price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $price_date_time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCertificateId(): ?Certificates
    {
        return $this->certificate_id;
    }

    public function setCertificateId(?Certificates $certificate_id): self
    {
        $this->certificate_id = $certificate_id;

        return $this;
    }

    public function getIssuingPrice(): ?float
    {
        return $this->issuing_price;
    }

    public function setIssuingPrice(float $issuing_price): self
    {
        $this->issuing_price = $issuing_price;

        return $this;
    }

    public function getCurrentPrice(): ?float
    {
        return $this->current_price;
    }

    public function setCurrentPrice(float $current_price): self
    {
        $this->current_price = $current_price;

        return $this;
    }

    public function getPriceDateTime(): ?\DateTimeInterface
    {
        return $this->price_date_time;
    }

    public function setPriceDateTime(\DateTimeInterface $price_date_time): self
    {
        $this->price_date_time = $price_date_time;

        return $this;
    }
}
