<?php

namespace App\Controller;

use App\BusinessLogic\Certificates\CertificatesInterface;
use App\BusinessLogic\Certificates\CertificatesService;
use App\BusinessLogic\CertificatesDocuments\CertificatesDocumentsService;
use App\BusinessLogic\PriceHistory\PriceHistoryService;
use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Dotenv\Exception\FormatException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CertificatesController extends AbstractController
{
    /**
     * @Route("/certificates", name="add_certificates")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addCertificatesPage()
    {
        return $this->render('certificates/addCertificate.html.twig');
    }

    /**
     * @Route("/certificates/manage", name="manage_certificates")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manageCertificatesMage(CertificatesService $certificateService)
    {
        $certificates = $certificateService->getAllCertificates();
        return $this->render('certificates/manageCertificate.html.twig', ['certificates' => $certificates]);
    }



    /**
     * @Route("/add", name="add")
     * @Method({"POST"})
     * @param Request $request
     * @param CertificatesService $certificateService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCertificates(Request $request, CertificatesService $certificateService)
    {
        $response = new JsonResponse();
        if ($request->files->get('document') && empty($request->get('document_type'))){
            $response->setData("Document type is required");
            $response->setStatusCode(403);

            return $response;
        }
        $params = $request->request->all();
        if ($request->files->get('document')) {
            $fileName = uniqid() . "." . $request->files->get('document')->guessExtension();
            $params['document_name'] = $fileName;
                $request->files->get('document')
                    ->move($this->getParameter('kernel.project_dir') . "/public/images", $fileName);
        }
        $result = $certificateService->addCertificates($params);
            $response->setData($result['data']);
            $response->setStatusCode($result['status']);

            return $response;

    }

    /**
     * @Route("/remove", name="remove")
     * @Method({"DELETE"})
     * @param Request $request
     * @param CertificatesService $certificateService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeCertificates(Request $request, CertificatesService $certificateService)
    {
        $result = $certificateService->removeCertificate($request->request->all());
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;

    }


    /**
     * @Route("certificates/display.{_format}",
     *     defaults={"_format": "html"},
     *     requirements={
     *         "_format": "html|xml"
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displayCertificates(Request $request, CertificatesService $certificateService)
    {
        $certificate = $certificateService->getCertificates($request->get('id'));
        $format = $request->getRequestFormat();
        if ($format == 'xml' && $certificate->getCertificateType() == 'guarantee-certificate'){

            throw new NotFoundHttpException('guarantee-certificate is not supported as xml');
        }

        return $this->render("certificates/certificate." . $format .".twig", ['certificate' => $certificate]);
    }

    /**
     * @Route("/certificate/add-price", name="add_price")
     * @Method({"POST"})
     * @param Request $request
     * @param CertificatesService $certificateService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCertificatePrices(Request $request, CertificatesService $certificateService, PriceHistoryService $priceHistory)
    {
        $response = new JsonResponse();
        if (empty($request->get('certificate_id'))){
            $response->setData("certificate id required");
            $response->setStatusCode(403);

            return $response;
        }
        $certificate = $certificateService->getCertificates($request->get('certificate_id'));
        if (empty($certificate)){
            $response->setData("certificate is required");
            $response->setStatusCode(404);

            return $response;
        }
        $params = array(
            'issuing_price' => $request->get('issuing_price'),
            'current_price' => $request->get('current_price')
        );
        $params['certificate'] = $certificate;
        $result = $priceHistory->addCertificatePrice($params);
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;

    }

    /**
     * @Route("/certificate/add-document", name="add-document")
     * @Method({"POST"})
     * @param Request $request
     * @param CertificatesService $certificateService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCertificateDocument(Request $request, CertificatesService $certificateService, CertificatesDocumentsService $documentService)
    {
        $response = new JsonResponse();
        if (empty($request->get('certificate_id'))){
            $response->setData("certificate id required");
            $response->setStatusCode(403);

            return $response;
        };
        if (!$request->files->get('document')){

            $response->setData("no document attached");
            $response->setStatusCode(403);

            return $response;
        }
        $certificate = $certificateService->getCertificates($request->get('certificate_id'));
        $fileName = uniqid() . "." . $request->files->get('document')->guessExtension();
        $request->files->get('document')
            ->move($this->getParameter('kernel.project_dir') . "/public/images", $fileName);
        $documentsParams = array(
            'name' => $fileName,
            'certificate' => $certificate,
            'document_type' => $request->get('document_type'),
            'path' => "images/" . $fileName
        );
        $result = $documentService->addDocument($documentsParams);
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;

    }
}
