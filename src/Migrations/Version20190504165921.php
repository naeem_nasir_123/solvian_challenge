<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190504165921 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE price_history DROP FOREIGN KEY FK_4C9CB81756E34EF2');
        $this->addSql('ALTER TABLE price_history ADD CONSTRAINT FK_4C9CB81756E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE certificate_documents DROP FOREIGN KEY FK_F6781CCD56E34EF2');
        $this->addSql('ALTER TABLE certificate_documents ADD CONSTRAINT FK_F6781CCD56E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE certificate_documents DROP FOREIGN KEY FK_F6781CCD56E34EF2');
        $this->addSql('ALTER TABLE certificate_documents ADD CONSTRAINT FK_F6781CCD56E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id)');
        $this->addSql('ALTER TABLE price_history DROP FOREIGN KEY FK_4C9CB81756E34EF2');
        $this->addSql('ALTER TABLE price_history ADD CONSTRAINT FK_4C9CB81756E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id)');
    }
}
