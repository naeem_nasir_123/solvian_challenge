<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 04.05.19
 * Time: 17:35
 */

namespace App\BusinessLogic\Certificates;


use Symfony\Component\HttpFoundation\Request;

interface CertificatesInterface
{
    public function addCertificates(array $params) :?array;
}