<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 03.05.19
 * Time: 16:03
 */

namespace App\BusinessLogic\Certificates;

use App\BusinessLogic\CertificatesDocuments\CertificatesDocumentsService;
use App\BusinessLogic\PriceHistory\PriceHistoryService;
use App\Repository\CertificatesRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use Symfony\Component\HttpFoundation\Request;

class CertificatesService implements CertificatesInterface
{
    /**
     * @var CertificatesRepository
     */
    private $certificatesRepository;

    /**
     * @var PriceHistoryService
     */
    private $priceHistoryService;

    /**
     * @var CertificatesDocumentsService
     */
    private $certificateDocumentService;

    /**
     * @ContainerInterface
     */
    private $container;

    /**
     * CertificatesService constructor.
     * @param CertificatesRepository $certificatesRepository
     * @param PriceHistoryService $priceHistoryService
     * @param CertificatesDocumentsService $certificatesDocumentsService
     * @param ContainerInterface $container
     */
    public function __construct(
        CertificatesRepository $certificatesRepository,
        PriceHistoryService $priceHistoryService,
        CertificatesDocumentsService $certificatesDocumentsService,
        ContainerInterface $container
    )
    {
        $this->certificatesRepository = $certificatesRepository;
        $this->priceHistoryService = $priceHistoryService;
        $this->certificateDocumentService = $certificatesDocumentsService;
        $this->container = $container;
    }

    /**
     * @param array $params
     * @return array|null
     */
    public function addCertificates(array $params) :?array
    {
        if (empty($params['isin']) || !is_numeric($params['isin'])){

            return array(
                'data' => 'isin is required and it must be a numeric value',
                'status' => 403
            );
        }
        if (empty($params['trading_market'])){

            return array(
                'data' => 'trading market is required',
                'status' => 403
            );
        }
        if (empty($params['currency'])){

            return array(
                'data' => 'currency is required',
                'status' => 403
            );
        }
        if (empty($params['issuer'])){

            return array(
                'data' => 'issuer is required',
                'status' => 403
            );
        }
        if (empty($params['issuing_price']) || is_numeric($params['issuing_price']) == false){

            return array(
                'data' => 'issuing price is required and it must be a number value',
                'status' => 403
            );
        }
        if (empty($params['current_price']) || is_numeric($params['current_price']) == false){

            return array(
                'data' => 'current price is required and must be number value',
                'status' => 403
            );
        }
        $fields = array(
            'isin' => $params['isin'],
            'trading_market' => $params['trading_market'],
            'issuer' => $params['issuer'],
            'currency' => $params['currency']
        );
        if (!empty($params['barrier_level']) && is_numeric($params['barrier_level']) == true) {
            $fields['barrier_level'] = $params['barrier_level'];
        }
        if (!empty($params['participation_rate']) && is_numeric($params['participation_rate']) == true) {
            $fields['participation_rate'] = $params['participation_rate'];
        }
        $result = $this->certificatesRepository->addCertificates($fields);
        if ($result['status'] == 201){
            $certificatePrice = array(
                'issuing_price' => $params['issuing_price'],
                'current_price' => $params['current_price'],
                'certificate' => $result['data']
            );
            $this->priceHistoryService->addCertificatePrice($certificatePrice);
            if (isset($params['document_name'])) {
                $documentsParams = array(
                    'name' => $params['document_name'],
                    'certificate' => $result['data'],
                    'document_type' => $params['document_type'],
                    'path' => "images/" . $params['document_name']
                );
                $this->certificateDocumentService->addDocument($documentsParams);
            }
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function removeCertificate(array $params)
    {
        if (empty($params['id'])){

            return array(
                'data' => 'id is required',
                'status' => 403
            );
        }
        $certificate = $this->certificatesRepository->find($params['id']);
        if (empty($certificate)){

            return array(
                'data' => 'certificate does not exist',
                'status' => 404
            );
        }
        foreach ($certificate->getCertificateDocuments() as $document){
            if (file_exists($this->container->get('kernel')->getProjectDir() . '/public/images/' . $document->getName())) {
                unlink($this->container->get('kernel')->getProjectDir() . '/public/images/' . $document->getName());
            }
        }

        return $this->certificatesRepository->removeCertificate($certificate);
    }

    /**
     * @param $id
     * @return \App\Entity\Certificates|null
     */
    public function getCertificates($id)
    {
        return $this->certificatesRepository->find($id);
    }

    /**
     * @return \App\Entity\Certificates[]
     */
    public function getAllCertificates()
    {
        return $this->certificatesRepository->findAll();
    }

}