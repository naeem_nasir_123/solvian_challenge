<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 04.05.19
 * Time: 00:32
 */

namespace App\BusinessLogic\PriceHistory;

use App\BusinessLogic\Certificates\CertificatesService;
use App\Entity\PriceHistory;
use App\Repository\PriceHistoryRepository;

class PriceHistoryService
{
    /**
     * @var PriceHistoryRepository
     */
    private $priceHistoryRepository;

    /**
     * PriceHistoryService constructor.
     * @param PriceHistoryRepository $priceHistpryRepository
     */
    public function __construct(PriceHistoryRepository $priceHistpryRepository)
    {
        $this->priceHistoryRepository = $priceHistpryRepository;
    }

    /**
     * @param array $params
     * @return array|null
     */
    public function addCertificatePrice(array $params) :?array
    {
        if (empty($params['current_price']) || is_numeric($params['current_price']) == false){

            return array(
                'data' => "current price is required and must be number value",
                'status' => 403
            );
        }
        if (empty($params['issuing_price']) || is_numeric($params['issuing_price']) == false){

            return array(
                'data' => "issuing price required and must be number value",
                'status' => 403
            );
        }

        return $this->priceHistoryRepository->addPrice($params);
    }

}