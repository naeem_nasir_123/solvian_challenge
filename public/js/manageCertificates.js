function addProfile() {

    var file_data =$('#document');
    var form_data = new FormData();
    form_data.append("document", file_data[0].files[0]) ;
    form_data.append("isin", $("#isin").val());
    form_data.append("trading_market", $("#trading_market").val());
    form_data.append("currency", $("#currency").val());
    form_data.append("issuer", $("#issuer").val());
    form_data.append("issuing_price", $("#issuing_price").val());
    form_data.append("current_price", $("#current_price").val());
    form_data.append("barrier_level", $("#barrier_level").val());
    form_data.append("participation_rate", $("#participation_rate").val());
    form_data.append("document_type", $("#document_type").val());
    $.ajax({
        url: "/add",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success:
            function(result)
            {
                new PNotify({
                    title: 'Added',
                    text: "Certificate Added Successfully",
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text:   result.responseJSON,
                    type: 'error'
                });
            }
    });
}

function removeCertificate(id) {

    $.ajax({
        method: "delete",
        url: "/remove",
        datatype: "json",
        data: { id: id},
        success: function(result) {
            new PNotify({
                title: 'Delete',
                text: result,
                type: 'success'
            });
            $("#"+id).addClass("hide");
        },
        error: function(result) {
            new PNotify({
                title: 'Delete',
                text: result.responseJSON,
                type: 'error'
            });
        }
    });

}

function addPrices() {

    var form_data = new FormData();
    form_data.append("certificate_id", $("#certificate_id").val());
    form_data.append("issuing_price", $("#issuing_price").val());
    form_data.append("current_price", $("#current_price").val());
    $.ajax({
        url: "/certificate/add-price",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success: function(result) {
            new PNotify({
                title: 'Added',
                text: result,
                type: 'success'
            });
            $("#"+id).addClass("hide");
        },
        error: function(result) {
            new PNotify({
                title: 'Failure',
                text: result.responseJSON,
                type: 'error'
            });
        }
    });
}

function addDocuments() {

    var file_data =$('#document');
    var form_data = new FormData();
    form_data.append("document", file_data[0].files[0]) ;
    form_data.append("certificate_id", $("#document_certificate_id").val());
    form_data.append("document_type", $("#document_type").val());
    $.ajax({
        url: "/certificate/add-document",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success: function(result) {
            new PNotify({
                title: 'Added',
                text: result,
                type: 'success'
            });
            $("#"+id).addClass("hide");
        },
        error: function(result) {
            new PNotify({
                title: 'Failure',
                text: result.responseJSON,
                type: 'error'
            });
        }
    });

}

$('#addPrices').on('show.bs.modal', function (e) {
    var myRoomNumber = $(e.relatedTarget).attr('data-id');
    alert(myRoomNumber);
    $("#certificate_id").val(myRoomNumber)
    $(this).find('.certificate_id').text(myRoomNumber);
});
$('#addDocument').on('show.bs.modal', function (e) {
    var myRoomNumber = $(e.relatedTarget).attr('data-id');
    $("#document_certificate_id").val(myRoomNumber)
});

$('#barrier_level').change(function () {
    $("#participation_rate").attr("disabled", true);
});
$('#participation_rate').change(function () {
    $("#barrier_level").attr("disabled", true);
});